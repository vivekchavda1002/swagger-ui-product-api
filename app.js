const express = require('express')
require('./database/connection')
const productRoute = require('./routes/productRoutes')
const app = express()
const swaggerUi = require('swagger-ui-express');
const YAML = require("yamljs");
const swaggerJSDocs = YAML.load("./api.yaml");

//swagger midddlew  res
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerJSDocs));

app.use(express.json())

//product Routes
app.use('/product',productRoute)
module.exports = app