const express = require('express')
const router = express.Router()
const productModel = require('../model/product.model')

// for adding new products.
router.post('/',async(req,res)=>{
    const newProduct = new productModel(req.body)
    try{
        await newProduct.save();
        res.status(201).send(newProduct)
    }catch(e){
        res.status(400).send(e)
    }
})

// for adding new products.
router.get('/',async(req,res)=>{    
    try{
        const allProducts = await productModel.find({})
        res.status(200).send(allProducts)
    }catch(e){
        res.status(400).send(e)
    }
})

//route for update product
router.patch('/:id',async(req,res)=>{    
    const updates = Object.keys(req.body)    
    const updateUser = await productModel.findById({_id:req.params.id})
    if(req.body.name){
        updateUser.name = req.body.name
    }
    if(req.body.qty){
        updateUser.qty = req.body.qty
    }
    if(req.body.available){
        updateUser.available = req.body.available
    }
    await updateUser.save()
    res.send(updateUser)

})
//route for deleting product
router.delete('/:id',async(req,res)=>{    
    try{
        const deletedProducts = await productModel.deleteOne({_id:req.params.id})
        res.status(204).send("Product Deleted")
    }catch(e){
        res.status(400).send(e)
    }
})

module.exports = router;