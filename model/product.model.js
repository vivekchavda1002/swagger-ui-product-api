const mongoose = require('mongoose')

const productSchema = new  mongoose.Schema({
    name:{
        type:String,
        require:true,
        trim:true
    },
    qty:{
        type:Number,
        require:true,
        trim:true
    },
    available:{
        type:Boolean,
        require:true,
        trim:true
    }
},{
    timestamps: true
})

module.exports = mongoose.model('product',productSchema)